package com.example.demo.kontrolery;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CarControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CarController carController;

    @BeforeEach
    private void setUp(){
        mockMvc= MockMvcBuilders.standaloneSetup(carController).build();
    }

    @Test
    void shouldReturnExpectedTest() throws Exception{
        final var mvcResult = mockMvc.perform(get("/api/cars"))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(mvcResult.getResponse().getStatus(), 200);
    }

    @Test
    void shouldReturnCar() throws Exception{
        final var mvcResult=mockMvc.perform(get("/api/cars"))
                .andExpect(status().isOk())
                .andReturn();
        assertTrue(mvcResult.getResponse().getContentAsString().contains("[{\"id\":1,\"brand\":\"brand\",\"model\":\"model\",\"regNum\":\"regNum\",\"user_owner\""));
    }


}
