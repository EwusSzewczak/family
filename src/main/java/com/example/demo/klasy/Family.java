package com.example.demo.klasy;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Family {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nazwisko;

    @OneToMany(mappedBy = "family1", cascade = CascadeType.ALL)
    private Set<User> user;

    public Family() {
    }

    public Family(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }
}
