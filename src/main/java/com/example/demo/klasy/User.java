package com.example.demo.klasy;


import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "FamilyMember")
public class User {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;

    private String name;

    private String surname;

    private Date birthdata;

    @OneToMany(mappedBy = "user_owner", cascade = CascadeType.ALL)
    private Set<Car> car;

    @ManyToOne(optional = false)
    private Family family1;

    public User() {
    }

    public User(String name, String surname, Date birthdata, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthdata = birthdata;
        this.family1=family;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthdata() {
        return birthdata;
    }

    public void setBirthdata(Date birthdata) {
        this.birthdata = birthdata;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthdata=" + birthdata +
                '}';
    }
}
