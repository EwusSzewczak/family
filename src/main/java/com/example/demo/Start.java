package com.example.demo;

import com.example.demo.klasy.Car;
import com.example.demo.klasy.Family;
import com.example.demo.klasy.User;
import com.example.demo.repo.CarRepo;
import com.example.demo.repo.FamilyRepo;
import com.example.demo.repo.UserRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Date;
import java.util.Set;

@Component
public class Start {
    private UserRepo userRepo;
    private CarRepo carRepo;
    private FamilyRepo familyRepo;

    @Autowired
    public Start(UserRepo userRepo, CarRepo carRepo, FamilyRepo familyRepo){

        this.userRepo=userRepo;
        this.carRepo=carRepo;
        this.familyRepo=familyRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runExample() {
        Date data1 = new Date();
        Family family = new Family("Szewczakowie");
        User user = new User("Mika", "Sz", data1, family);
        User user1 = new User("Kiki", "Sz", data1, family);
        Car car = new Car("Volkswagen", "Golf", "KNS 9662C", user);

        familyRepo.save(family);
        userRepo.save(user);
        userRepo.save(user1);
        carRepo.save(car);
        Iterable<User> all = userRepo.findByFirstnameEndsWith();
        all.forEach(System.out::println);
    }

}
