package com.example.demo.repo;

import com.example.demo.klasy.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CarRepo extends JpaRepository<Car, Long> {
}
