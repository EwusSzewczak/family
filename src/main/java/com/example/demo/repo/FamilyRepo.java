package com.example.demo.repo;

import com.example.demo.klasy.Family;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyRepo extends JpaRepository<Family, Long> {

}
