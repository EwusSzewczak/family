package com.example.demo.repo;

import com.example.demo.klasy.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends JpaRepository<User, Long> {
    @Query("select u from User u where  u.name like '%a'")
    List<User> findByFirstnameEndsWith();
}
