package com.example.demo.kontrolery;

import com.example.demo.klasy.User;
import com.example.demo.repo.UserRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(headers = "Access-Control-Allow-Origin")
public class UserController {
    private final UserRepo userRepo;
    UserController(UserRepo userRepo){

        this.userRepo = userRepo;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/users")
    List<User> all()
    {
        return  userRepo.findAll();
    }

    @PostMapping("/users")
    User newUser(@RequestBody User newUser){
        return userRepo.save(newUser);
    }


    @GetMapping("/users_ends_a")
    List<User>ends_a(){
        return userRepo.findByFirstnameEndsWith();
    }
}
