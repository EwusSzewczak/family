package com.example.demo.kontrolery;

import com.example.demo.klasy.Family;
import com.example.demo.repo.FamilyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FamilyController {
    @Autowired
    private FamilyRepo familyRepo;

    @PostMapping("/family")
    public Family newFamily(@RequestBody Family newFamily){
        return (Family) familyRepo.save(newFamily);
    }

    @GetMapping("/family")
    List<Family> all(){
        return familyRepo.findAll();
    }


}
