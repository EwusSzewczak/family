package com.example.demo.kontrolery;

import com.example.demo.klasy.Car;
import com.example.demo.klasy.User;
import com.example.demo.repo.CarRepo;
import com.example.demo.repo.UserRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/")
public class CarController {
    private final CarRepo carRepo;

    public CarController(CarRepo carRepo) {
        this.carRepo = carRepo;
    }

    @GetMapping("/cars")
    List<Car> all(){
        return carRepo.findAll();
    }

    @PostMapping("/cars")
    Car newCar(@RequestBody Car newCar){
        return carRepo.save(newCar);
    }

}
